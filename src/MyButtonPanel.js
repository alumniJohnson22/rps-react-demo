import React from 'react';
import MyButton from './MyButton';
import './App.css';

class MyButtonPanel extends React.Component {

    constructor(props) {
      super(props);
    }

    handleClick(event) {
        this.props.handleClick(event);
      }

    generateButton(element, index) {
        return (
            <MyButton name={element} index={index} handleClick={this.props.handleClick}/>
        );
    }

    generateAllButtons() {
        return (
            this.props.gameChoices.map((element, index)=> {
                return this.generateButton(element, index)}
                )
            
        );
    }

    render() {
        return (
            <div>Hello, MyButtonPanel
                <div>
                    {this.generateAllButtons()}
                </div>
            </div>
            
        );
    }
}
export default MyButtonPanel