import React from 'react';
import './App.css';
import MyButtonPanel from './MyButtonPanel';

class App extends React.Component {

  constructor(props) {
    super(props);
    this.gameChoices = ['rock', 'paper', 'scissors', 'fourth'];
    this.state = {
      clientChoice: null,
      clientChoiceMsg: null,
      serverChoice: null,
      serverChoiceMsg: null,
      resultOfGame: null,
      resultOfGameMsg: null
    }
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(event) {
    let localState = this.state;
    localState.clientChoiceMsg = "Your selection is " + event.target.name;
    let index = Math.floor(Math.random() * this.gameChoices.length);
    localState.serverChoice = index;
    localState.serverChoiceMsg = "Player 2 chose " + this.gameChoices[index];
    localState.clientChoice = this.gameChoices.indexOf(event.target.name);
  
    if (localState.serverChoice === localState.clientChoice) {
      localState.resultOfGameMsg = 'Tie!'
    }
    else if (localState.clientChoice === (localState.serverChoice + 1) % this.gameChoices.length) {
      localState.resultOfGameMsg = 'You win!'
    }
    else {
      localState.resultOfGameMsg = 'Player 2 wins!'     
    }
    this.setState(localState);
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          Rock Paper Scissors
        </header>
        <div name="my-buttons"> 
          <MyButtonPanel gameChoices={this.gameChoices} handleClick={this.handleClick}/>
        </div>
        <div name="client-choice">{this.state.clientChoiceMsg || "Your Selelction?"}</div>
        <div name="server-choice">{this.state.serverChoiceMsg || "Waiting for Player 2"}</div>
        <div name="result-of-game">{this.state.resultOfGameMsg || "Who will win?"}</div>
      </div>
      
    );
  }
}

export default App;
