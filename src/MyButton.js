import React from 'react';
import './App.css';

class MyButton extends React.Component {

    constructor(props) {
      super(props);
    }

    handleClick(event) {
        this.props.handleClick(event)
    }
    render() {
        return (
            <div name="my-button">
            <button 
                name={this.props.name} 
                index={this.props.index} 
                onClick={(event)=>{this.handleClick(event)}}
            >
                {this.props.name}
            </button>
            </div>
        );
    }
}
export default MyButton